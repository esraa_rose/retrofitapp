package com.vogella.retrofitgerrit;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.List;

public class recycleAdapter  extends RecyclerView.Adapter<recycleAdapter.ViewHolder> {

    public List<Item> MyItems;

    public recycleAdapter() {
       // this.MyItems = MyItems;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycleview_row, parent, false);
        return new ViewHolder(view);
    }
//
//@Override
//public void onBindViewHolder(ViewHolder holder, int position) {
//    final MyListData myListData = listdata[position];
//    holder.textView.setText(listdata[position].getDescription());
//    holder.imageView.setImageResource(listdata[position].getImgId());
//    holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
//        @Override
//        public void onClick(View view) {
//            Toast.makeText(view.getContext(),"click on item: "+myListData.getDescription(),Toast.LENGTH_LONG).show();
//        }
//    });
//}
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        Item item = MyItems.get(position);
//        holder.Titletv.setText(MyItems.get(position).getTitle());
//        holder.imageView.setImageResource(MyItems.get(position).getMedia().getM());

        try {
            Glide.with(holder.itemView.getContext())
                    .load(item.getMedia().getM())
                    .into(holder.imageView);

            holder.Titletv.setText(item.getTitle());

            holder.parentLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(view.getContext(),"click on item: "+MyItems.get(position).getTitle(),Toast.LENGTH_LONG).show();
                }
            });


        } catch (java.lang.NullPointerException exception) {
            Toast.makeText(holder.itemView.getContext(), "", Toast.LENGTH_LONG).show();
        }





    }


//
    @Override
    public int getItemCount() {
        return  MyItems.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout parentLayout;
        ImageView imageView;
        TextView Titletv;
        ViewHolder(View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.image);
            parentLayout = itemView.findViewById(R.id.linearlayot);
            Titletv = itemView.findViewById(R.id.title);


        }
    }
}
