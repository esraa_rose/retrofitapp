package com.vogella.retrofitgerrit;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {
    @GET("services/feeds/photos_public.gne?tagmode=all&&format=json&&nojsoncallback=1")
    public Call<Example> getPhoto();
}
